package com.rishat.shamsutdinov.servicepractice

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.widget.Toast
import androidx.core.app.NotificationCompat


class ServiceExp : Service() {

    private val CHANNEL_ID = "com.rishat.shamsutdinov.ServicePractice"
    private val STATUS_KEY = "status"
    private val RUN_AS_BACKGROUND = 12
    private val RUN_AS_FOREGROUND = 24
    private val mBinder = ExpBinder()
    private var serviceStatus = 0


    override fun onBind(intent: Intent): IBinder {
        showToast("Service Bounded")
        startService(RUN_AS_BACKGROUND)
        return mBinder
    }

    inner class ExpBinder : Binder() {
        val service: ServiceExp
            get() = this@ServiceExp
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val handler = Handler()
        if (intent != null) {
            serviceStatus = intent.getIntExtra(STATUS_KEY, 0)
        }
        when (serviceStatus) {
            RUN_AS_BACKGROUND -> {
                handler.postDelayed({ startService(RUN_AS_FOREGROUND) }, 10000)
                showToast("Run as background")
            }
            RUN_AS_FOREGROUND -> {
                startNotification()
                showToast("Run as foreground")
                handler.postDelayed({ stopSelf() }, 10000)
            }
            else -> showToast("Don't know what is actually running")
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        showToast("Service Unbounded")
        return super.onUnbind(intent)
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    private fun startService(type: Int) {
        val intent = Intent(this, ServiceExp::class.java)
        intent.putExtra(STATUS_KEY, type)
        startService(intent)
    }

    override fun onDestroy() {
        showToast("Service destroyed")
        super.onDestroy()
    }

    private fun startNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, "Something", importance)
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("A service is running in the background")
            .setContentText("Actually running").build()
        startForeground(1421, notification)
    }
}
