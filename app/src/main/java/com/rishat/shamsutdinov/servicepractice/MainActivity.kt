package com.rishat.shamsutdinov.servicepractice

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var serviceIntent: Intent
    private var mService: ServiceExp? = null
    private var mIsBound = false

    private val serviceConnection = object : ServiceConnection {

        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as ServiceExp.ExpBinder
            mService = binder.service
            mIsBound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            mIsBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        serviceIntent = Intent(this, ServiceExp::class.java)
        stopService(serviceIntent)

        btn_bind_to_service.setOnClickListener {
            bindService()
        }

        btn_unbind_from_service.setOnClickListener {
            unBindService()
        }
    }

    private fun bindService() {
        if (!mIsBound) {
            bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        unBindService()
        super.onStop()
    }

    private fun unBindService() {
        if (mIsBound) {
            mIsBound = false
            unbindService(serviceConnection)
        }
    }
}